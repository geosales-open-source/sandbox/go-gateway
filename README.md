# go-gateway

## Descrição

## Primerios passos


### Fastest GET using Golang

1 Usando *cheat* para fazer http request em Golang.
Usei uma REPL para GO.  
https://github.com/x-motemen/gore#installation

Comandos para instalar:
```bash
$ go install github.com/x-motemen/gore/cmd/gore@latest
$ go install github.com/mdempsky/gocode@latest   # for code completion
```

Para iniciar a REPL  
`$ gore`

2- Efetuar GET
```
gore> :import "net/http"
gore> resp, err := http.Get("http://httpbin.org/get")
gore> resp
```
>Obs: existe uma syntax própria do gore como o *:import*

3- Resposta mais bonitinha
Vamos usar duas `dependências`
`io/ioutil` e `log`

```go
gore> :import "net/http"
gore> resp, err := http.Get("http://httpbin.org/get")

gore> :import  "io/ioutil"
gore> :import  "log"

gore> body, err := ioutil.ReadAll(resp.Body)
gore> sb := string(body)

/* Que vai retornar

2022/07/07 16:14:10 {
  "args": {},
  "headers": {
    "Accept-Encoding": "gzip",
    "Host": "httpbin.org",
    "User-Agent": "Go-http-client/1.1",
    "X-Amzn-Trace-Id": "Root=1-62c73082-7fcc688f3259c14b7861aedf"
  },
  "origin": "189.85.33.141",
  "url": "http://httpbin.org/get"
}

*/
```
### Servidor simples usando GORE
Para um servidor simples é suficiente duas dependências *fmt* e *net/http*

```go
$ gore
//gore version 0.5.5  :help for help
gore> :import "fmt"
gore> :import "net/http"
gore> http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) { fmt.Println("hello world") })
gore> http.ListenAndServe(":8081", nil)
```

Basta fazer um `curl localhost:8081/` e *hello world* é impresso no console do *Gore*


### Pequena Observação sobre Funções em Go

Funções em Go podem confundir um pouco, por exemplo como foi feito em
```go
resp, err := http.Get("http://httpbin.org/get")`
```

aqui eu imagino estar atribuido às duas variaveis `resp` e `err` isso fica mais claro 
quando observamos o tipo de retorno da função que é `(resp *Response, err error)`

https://pkg.go.dev/net/http#Get  
Assinatura da função
```go
func Get(url string) (resp *Response, err error)
```



