package main

import (
	"fmt"
	"net/http"
	"time"
)

func main() {
	originServerHandler := http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		fmt.Printf("[Simple Server] received request at: %s\n", time.Now())
		fmt.Fprint(rw, "Origin Server Response Message")
	})

	http.ListenAndServe(":8081", originServerHandler)
}