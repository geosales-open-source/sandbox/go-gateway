package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"time"
)

func main() {
	http.HandleFunc("/google", reverseProxyOf("http://google.com/"))
	http.HandleFunc("/yandex", reverseProxyOf("http://yandex.com/"))
	http.HandleFunc("/post/httpbin", reverseProxyOf("http://httpbin.org/post"))
	http.HandleFunc("/delete/httpbin", reverseProxyOf("http://httpbin.org/delete"))

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal("internal server error")
	}
}

func reverseProxyOf(urlRedirect string) http.HandlerFunc {
	return func(rw http.ResponseWriter, req *http.Request) {
		originServerURL, err := url.Parse(urlRedirect)
		if err != nil {
			log.Fatal("invalid origin server URL")
		}

		fmt.Printf("[Reverse Proxy] received request at: %s\n", time.Now())

		req.Host = originServerURL.Host
		req.URL.Host = originServerURL.Host
		req.URL.Scheme = originServerURL.Scheme
		req.URL.Path = originServerURL.Path
		req.RequestURI = ""

		originServerResponse, errr := http.DefaultClient.Do(req)

		if errr != nil {
			fmt.Printf("error: %s\n", errr)
		} else {
			rw.WriteHeader(originServerResponse.StatusCode)

			assignHeader(rw, originServerResponse.Header)

			fmt.Print(originServerResponse.Header)
			io.Copy(rw, originServerResponse.Body)
		}
	}
}

func assignHeader(rw http.ResponseWriter, header http.Header) {
	for k, vs := range header {
		for _, v := range vs {
			rw.Header().Add(k, v)
		}
	}
}
